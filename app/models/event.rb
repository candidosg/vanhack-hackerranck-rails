class Event < ApplicationRecord
    default_scope { order(id: :asc) }

    validates :id, presence: true, uniqueness: true
    validates :event_type, presence: true

    belongs_to :actor, touch: true
    belongs_to :repository, touch: true
end