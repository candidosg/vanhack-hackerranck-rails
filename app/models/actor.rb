class Actor < ApplicationRecord
    default_scope { order(created_at: :desc) }

    validates :id, uniqueness: true
    validates :login, :avatar_url, presence: true
    validates :avatar_url, url: true

    has_many :events
end