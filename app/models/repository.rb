class Repository < ApplicationRecord
    validates :id, uniqueness: true
    validates :name, :url, presence: true
    validates :url, url: true
end