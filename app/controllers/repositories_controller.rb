class RepositoriesController < ApplicationController
    before_action :set_repository, only: [:show, :update, :destroy]
  
    def index
      repositories = Repository.all
      json_response(repositories)
    end
  
    def show
      json_response(@repository)
    end
  
    def create
      @repository = Repository.new(repository_params)
      if @repository.save
        json_response(@repository, :created)
      else
        json_response(@repository.errors, :unprocessable_entity)
      end
    end
  
    def update
      if @repository.update(repository_params)
        json_response(@repository)
      else
        json_response(@repository.errors, :unprocessable_entity)
      end
    end
  
    def destroy
      @repository.destroy
      head :no_content
    end
  
    private
    def set_repository
      @repository = Repository.find(params[:id])
    end
  
    def repository_params
      params.permit(:name, :url)
    end
end