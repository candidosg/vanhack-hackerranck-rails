class ActorsController < ApplicationController
  before_action :set_actor, only: [:show, :update, :destroy]

  def index
    # @actors = Actor.left_joins(:events).group(:id).order('COUNT(events.id) DESC')
    # @actors = Actor.joins(:events).order('count(events.id) DESC')
    @actors = Actor.joins(:events).group("events.actor_id").order("count(events.actor_id) desc, events.created_at desc")
    json_response(@actors)
  end

  def show
    json_response(@actor)
  end

  def create
    @actor = Actor.new(actor_params)
    if @actor.save
      json_response(@actor, :ok)
    else
      json_response(@actor.errors, :unprocessable_entity)
    end
  end

  def update
    if @actor.update(actor_params)
      json_response(@actor)
    else
      json_response(@actor.errors, :unprocessable_entity)
    end
  end

  def destroy
    @actor.destroy
    head :no_content
  end

  private
  def set_actor
    @actor = Actor.find(params[:id])
  end

  def actor_params
    params.permit(:id, :login, :avatar_url)
  end
end