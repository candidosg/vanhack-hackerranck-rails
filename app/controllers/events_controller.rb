class EventsController < ApplicationController
    before_action :set_event, only: [:show, :update, :destroy]
  
    def index
      @events = Event.all
      json_response(@events)
    end
  
    def show
      json_response(@event)
    end
  
    def create
      actor = Actor.new(event_params[:actor])
      repository = Repository.new(event_params[:repository])

      @event = Event.new({
          id: event_params[:id],
          event_type: event_params[:event_type],
          created_at: event_params[:created_at],
          actor: actor,
          repository: repository
      })
      
      if @event.save
        json_response(@event, :created)
      else
        json_response(@event.errors, :bad_request)
      end
    end
  
    def update
      if @event.update(event_params)
        json_response(@event)
      else
        json_response(@event.errors, :unprocessable_entity)
      end
    end
  
    def destroy
      @event.destroy
      head :no_content
    end

    def destroy_all
      Event.delete_all
      Actor.delete_all
      Repository.delete_all
      json_response('')
    end

    def actors
      @event = Event.where(actor: params[:actor_id])
      if @event.length > 0
        json_response(@event)
      else
        json_response('', :not_found)
      end
    end
  
    private
    def set_event
      @event = Event.find(params[:id])
    end
  
    def event_params
      params[:event_type] = params[:type]
      params[:repository] = params[:repo]

      params.permit(:id, :event_type, :created_at, repository: [:id, :name, :url], actor: [:id, :login, :avatar_url])
    end
  end