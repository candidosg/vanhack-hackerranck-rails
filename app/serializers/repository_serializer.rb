class RepositorySerializer < ActiveModel::Serializer
    attributes :id, :name, :url
end