class EventSerializer < ActiveModel::Serializer
    attributes :id, :created_at
    attribute :event_type, key: :type

    belongs_to :repository, serializer: RepositorySerializer, key: :repo
    belongs_to :actor, serializer: ActorSerializer

    def created_at
        object.created_at.strftime("%Y-%m-%d %H:%M:%S")
    end
end