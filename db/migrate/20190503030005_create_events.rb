class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :event_type
      t.references :actor, index: true 
      t.references :repository, index: true 
      t.timestamps
    end
  end
end
