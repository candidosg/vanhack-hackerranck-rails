Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/actors/streak', to: 'actors#index'
  get '/events/actors/:actor_id', to: 'events#actors'
  put '/actors', to: 'actors#create'
  delete '/erase', to: 'events#destroy_all'

  resources :actors, :events, :repositories
end
